import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

export class Vragen{
  constructor(public vraag1: string)
  {

  }
}

export class Customer{
  constructor(public firstName: string, public lastName: string, public birthDate: string, public eMail: string, public vraag1: string)
  {

  }
}

@Injectable()
export class DataService {
  private CUSTOMER: Customer;

  httpObs: Observable<any>;


  constructor(private http: HttpClient, private route: Router, private activatedRoute: ActivatedRoute) {
    this.CUSTOMER = new Customer('','','','','');
  }


  getCustomers() {
    this.httpObs = this.http.get('/api/customers');
    return this.httpObs;
  }

  setCustomers(newCustomer: Customer) {
    this.CUSTOMER.firstName = newCustomer.firstName;
    this.CUSTOMER.lastName = newCustomer.lastName;
    this.CUSTOMER.birthDate = newCustomer.birthDate;
    this.CUSTOMER.eMail = newCustomer.eMail;
    console.log(this.CUSTOMER);
    //dit moet eerst lokaal gebeuren
  }

  deleteCustomers (id) {
    this.httpObs = this.http.delete('/api/customer/' + id + '');
    this.httpObs.subscribe(
      (data) => {
        console.log(data);
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      },
      () => {
        console.log('deleted');
      }
    );
  }

  setAll(newAntwoorden: Vragen) {
    this.CUSTOMER.vraag1 = newAntwoorden.vraag1;
    this.httpObs = this.http.post('/api/customer',
      JSON.stringify(this.CUSTOMER),
      {headers: new HttpHeaders().set('Content-Type','application/json')}
    );
    this.httpObs.subscribe(
      (data) => {
        console.log(data);
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    );
  }

  updateCustomers(updatecustomer: Customer, id ) {
    this.CUSTOMER.firstName = updatecustomer.firstName;
    this.CUSTOMER.lastName = updatecustomer.lastName;
    this.CUSTOMER.birthDate = updatecustomer.birthDate;
    this.CUSTOMER.eMail = updatecustomer.eMail;
    this.CUSTOMER.vraag1 = updatecustomer.vraag1;
    this.httpObs = this.http.put('/api/customer/' + id + '',
      JSON.stringify(this.CUSTOMER),
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}
      );
    this.httpObs.subscribe(
      (data) => {
        console.log(data);
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      },
      () => {
        console.log('updated');
      }
    );
  }
}

