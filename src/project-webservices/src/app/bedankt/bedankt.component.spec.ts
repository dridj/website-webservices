import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BedanktComponent } from './bedankt.component';

describe('BedanktComponent', () => {
  let component: BedanktComponent;
  let fixture: ComponentFixture<BedanktComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BedanktComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BedanktComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
