import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Customer, DataService } from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'user-info-form',
  templateUrl: './user-info-form.component.html',
  styleUrls: ['./user-info-form.component.css']
})
export class UserInfoFormComponent implements OnInit {
  klant: Customer;
  buffer: string;
  constructor(private data: DataService, private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  onNext(personalInfo: NgForm) {

    console.log("hallo");
    this.klant = new Customer(personalInfo.value.firstName,
      personalInfo.value.lastName,
      personalInfo.value.birthDate,
      personalInfo.value.eMail,
      this.buffer);
    this.data.setCustomers(this.klant);

    console.log(this.data.getCustomers());
    console.log(this.klant);
    //routerlink to feedback!
    this.route.navigate(['feedback'], {relativeTo:this.activatedRoute});
  }

  gotoshowcustomer() {
    this.route.navigate(['showcustomer'], {relativeTo:this.activatedRoute});
  }


}
