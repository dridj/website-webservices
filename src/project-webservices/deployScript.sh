#!/bin/bash
LARAVEL_DIR="/home/dries/MEGA/MEGAsync/school/semester5/webservices/website-webservices/laravel-project/"
echo 'Deploying Angular project with / as base'
ng build --prod --aot
echo 'copy dist to Laravel'
cp dist/* ${LARAVEL_DIR}"/public"
echo 'Copy ${LARAVEL_DIR}/public/index.html to ${LARAVEL_DIR}/resources/views/index.php'
mv ${LARAVEL_DIR}"/public/index.html" ${LARAVEL_DIR}"/resources/views/index.php"
echo
echo 'Done! You still have to add the Laravel routes in ${LARAVEL_DIR}/routes/web.php'
echo