<?php

use Illuminate\Http\Request;

Route::post('/customer', [
    'uses' => 'CustomerController@postCustomer'

]);

Route::get('/customers',[
    'uses' => 'CustomerController@getCustomers'

]);

Route::put('/customer/{id}',[
    'uses' => 'CustomerController@putCustomer'
]);

Route::delete('/customer/{id}',[
    'uses' => 'CustomerController@deleteCustomer'
]);
