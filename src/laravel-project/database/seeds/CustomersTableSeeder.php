<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 15; $i++)
        {
            App\Customer::insert([
                'firstName' => 'voornaam'.$i,
                'lastName' => 'achternaam'.$i,
                'birthDate' => ''.rand(1,31).'-'.rand(1,12).'-'.rand(1910,2000),
                'eMail' => 'Email'.$i.'@adres.com',
                'vraag1' => 'randomantwoord'.$i
            ]);
        }
    }
}
