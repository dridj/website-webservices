<?php
namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller {
    public function postCustomer(Request $request)
    {
        $customer = new Customer();
        $customer->timestamps = false;
        $customer->firstName = $request->input('firstName');
        $customer->lastName = $request->input('lastName');
        $customer->birthDate = $request->input('birthDate');
        $customer->eMail = $request->input('eMail');
        $customer->vraag1 = $request->input('vraag1');
        $customer->id = $request->input('id');
        $customer->save();
        return response()->json(['customer' => $customer], 201);
    }

    public function getCustomers()
    {
        $customers = Customer::all();
        $response =  $customers;
        return response()->json($response,200);
    }

    public function putCustomer(Request $request, $id)
    {
        $customer = Customer::find($id);
        if(!$customer){
            return response()->json(['message' => 'customer not found'], 404);
        }
        $customer->timestamps = false;
        $customer->firstName = $request->input('firstName');
        $customer->lastName = $request->input('lastName');
        $customer->birthDate = $request->input('birthDate');
        $customer->eMail = $request->input('eMail');
        $customer->vraag1 = $request->input('vraag1');
        $customer->save();
        return response()->json(['customer' => $customer],200);
    }

    public function deleteCustomer($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return response()->json(['message' => 'customer deleted'],200);
    }
}