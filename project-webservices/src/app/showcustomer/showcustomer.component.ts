import { Component, OnInit } from '@angular/core';
import { Customer, DataService} from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'showcustomer',
  templateUrl: './showcustomer.component.html',
  styleUrls: ['./showcustomer.component.css']
})
export class ShowcustomerComponent implements OnInit {
  CUSTOMER: Customer;

  constructor(private data: DataService, private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.data.getCustomers().subscribe(
      (data) => {
        this.CUSTOMER = data;
      }
    )
  }

  deletecustomer(id) {
    this.data.deleteCustomers(id);
  }

  updatecustomer(id){
    this.route.navigate(['/updatecustomer/'+id+''], {relativeTo:this.activatedRoute});
  }

}
