import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Vragen, DataService} from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  antwoorden: Vragen;

  constructor(private data: DataService, private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  onSubmit (feedbackInfo: NgForm) {
    this.antwoorden = new Vragen(feedbackInfo.value.vraag1);
    console.log(this.antwoorden);
    this.data.setAll(this.antwoorden);
    this.route.navigate(['/bedankt'], {relativeTo:this.activatedRoute});
  }
}
