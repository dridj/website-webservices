import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { UserInfoFormComponent } from './user-info-form/user-info-form.component';
import {Customer, DataService} from './data.service';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { HttpClientModule } from '@angular/common/http';
import { ShowcustomerComponent } from './showcustomer/showcustomer.component';
import { BedanktComponent } from './bedankt/bedankt.component';
import { UpdatecustomerComponent } from './updatecustomer/updatecustomer.component';

const appRoutes: Routes = [
  { path: '', component:  UserInfoFormComponent},
  { path: 'feedback', component: FeedbackComponent},
  { path: 'adminlogin', component: AdminloginComponent},
  { path: 'showcustomer', component: ShowcustomerComponent},
  { path: 'bedankt', component: BedanktComponent},
  { path: 'updatecustomer/:id' , component: UpdatecustomerComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    UserInfoFormComponent,
    AdminloginComponent,
    FeedbackComponent,
    ShowcustomerComponent,
    BedanktComponent,
    UpdatecustomerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
