import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Customer, DataService } from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-updatecustomer',
  templateUrl: './updatecustomer.component.html',
  styleUrls: ['./updatecustomer.component.css']
})
export class UpdatecustomerComponent implements OnInit {
  klant: Customer;
  constructor(private data: DataService, private route: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

  }

  onUpdate(personalInfo: NgForm) {

    console.log("hallo");
    this.klant = new Customer(personalInfo.value.firstName,
      personalInfo.value.lastName,
      personalInfo.value.birthDate,
      personalInfo.value.eMail,
      personalInfo.value.vraag1)
    this.data.updateCustomers(this.klant,this.activatedRoute.snapshot.params['id']);

    console.log(this.data.getCustomers());
    console.log(this.klant);
    //routerlink to feedback!
    this.route.navigate(['/showcustomer'], {relativeTo:this.activatedRoute});
  }

}
