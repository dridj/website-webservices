<?php
Route::get('/', function () {
    return View::make('index');
});

Route::any('{catchall}', function() {
	return View::make('index');
})->where('catchall','.*');
